/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.uzdiz.nsinjori.dz3.Aplikacija;

import org.foi.uzdiz.nsinjori.dz3.CoR.Chain;
import org.foi.uzdiz.nsinjori.dz3.CoR.Komande;
import org.foi.uzdiz.nsinjori.dz3.CoR.isprazniKomanda;
import org.foi.uzdiz.nsinjori.dz3.CoR.kontrolaKomanda;
import org.foi.uzdiz.nsinjori.dz3.CoR.kreniKomanda;
import org.foi.uzdiz.nsinjori.dz3.CoR.kvarKomanda;
import org.foi.uzdiz.nsinjori.dz3.CoR.pripremiKomanda;
import org.foi.uzdiz.nsinjori.dz3.CoR.statusKomanda;
import org.foi.uzdiz.nsinjori.dz3.Composite.Podrucje;
import org.foi.uzdiz.nsinjori.dz3.FactoryMethod.FileReader;
import org.foi.uzdiz.nsinjori.dz3.FactoryMethod.ReaderFactory;
import org.foi.uzdiz.nsinjori.dz3.Podaci.Spremnik;
import org.foi.uzdiz.nsinjori.dz3.Podaci.SpremnikKategorija;
import org.foi.uzdiz.nsinjori.dz3.Podaci.Ulica;
import org.foi.uzdiz.nsinjori.dz3.Podaci.Vozilo;
import org.foi.uzdiz.nsinjori.dz3.Singleton.Ispis;
import org.foi.uzdiz.nsinjori.dz3.Singleton.Parametri;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import org.foi.uzdiz.nsinjori.dz3.MVC.View;

/**
 *
 * @author Nikola
 */
public class nsinjori_zadaca_3 {
    /**
     * @param args the command line arguments
     */
    public static int brojac = 0;
    public static String parameterFileName;
    public static List<Vozilo> vozila = new ArrayList<>();
    public static List<Vozilo> listaParkiranihVozila = new ArrayList<>(vozila);
    public static List<Vozilo> listaPripremljenihVozila = new ArrayList<>();
    public static List<Vozilo> listaPokvarenihVozila = new ArrayList<>();
    public static List<Ulica> ulice = new ArrayList<>();
    
    //za conemu gornji prozor
    private static List<String> datoteka_zapisi = new ArrayList<>();
    
    
    
    public static void main(String[] args) {
        
        int brg = -1;
        boolean brgB = false;
        
        int brd = -1;
        boolean brdB = false;
        
        int sirina = -1;
        boolean sirinaB = false;
        
        System.out.println("Broj parametara " + args.length);
        datoteka_zapisi.add("Bok?");
        
        if(args.length==5){
            
            for (int i = 0; i < args.length; i++) {
                switch (args[i]) {
                    case "--brg":
                        i++;
                        if (i == args.length) {
                        } else {
                            int s = Integer.parseInt(args[i]);
                            if (s > 0) {
                                brgB = true;
                                brg = s;
                            } else {
                            }
                        }
                        break;
                    case "--brd":
                        i++;
                        if (i == args.length) {
                        } else {
                            int s = Integer.parseInt(args[i]);
                            if (s > 0) {
                                brdB = true;
                                brd = s;
                            }
                        }
                        break;
                    default:
                        /*return*/;
                }
            }

            if (brgB == false) {
                brg = 20;
            }
            if (brdB == false) {
                brd = 5;
            }
            if (sirinaB == false) {
                sirina = 137;
            }
            
            System.out.println("brg " + brg);
            System.out.println("brd " + brd);
            
            new View(20, 5);
            
            //System.exit(0);
            
            Parametri.getInstance().readParameterFile(args[0]);
            ReaderFactory rf;
            FileReader fr;
            List<String[]> listaUlica = citajUlice();
            citajSpremnici();
            citajVozila();
            kreirajUliceDodajKorisnike(listaUlica);      
            kreirajPodrucja();
            if(Parametri.isStatusCitanja()) ispisiOtpad();
            chainDispecer();
            for(Vozilo v: listaPripremljenihVozila){
                if(v.trenutnaKolicina>0) v.iprazniKamion();
            }
        }else{
            System.out.println("Krivi broj parametara. Potreno je navesti datoteku parametri.txt te parametre -brg i -brd");
        }
    }

    private static void chainDispecer() {
              
        ReaderFactory rf;
        FileReader fr;
        //dispečerijo druga verzija
        Chain chainKomanda1 = new pripremiKomanda();
        Chain chainKomanda2 = new kreniKomanda();
        Chain chainKomanda3 = new kvarKomanda();
        Chain chainKomanda4 = new statusKomanda();
        Chain chainKomanda5 = new isprazniKomanda();
        Chain chainKomanda6 = new kontrolaKomanda();
        chainKomanda1.setNextChain(chainKomanda2);
        chainKomanda2.setNextChain(chainKomanda3);
        chainKomanda3.setNextChain(chainKomanda4);
        chainKomanda4.setNextChain(chainKomanda5);
        chainKomanda5.setNextChain(chainKomanda6);
        rf = new ReaderFactory("dispecer");
        fr = rf.fileReader();
        List<String[]> listaDispecera = fr.getRecords();
        for(String[] zapisiDispecera : listaDispecera){
            Komande request = new Komande(String.join(";", zapisiDispecera));
            chainKomanda1.obradiKomandu(request);
        }
        //stari dispecer
    }

    private static void kreirajPodrucja() {
        ReaderFactory rf;
        FileReader fr;
        //kreiraj podrucja
        rf = new ReaderFactory("podrucja");
        fr = rf.fileReader();
        List<String[]> listaPodrucja = fr.getRecords();
        List<Podrucje> podrucja = new ArrayList<>();
        for(String[] zapisiPodrucja : listaPodrucja){
            podrucja.add(new Podrucje(zapisiPodrucja[0],zapisiPodrucja[1],zapisiPodrucja[2]));
        }
        for (Podrucje p : podrucja) p.poveziPodrucja();
        for (Podrucje p : podrucja) Ispis.getInstance().ispisZaDatoteku(p.ispisStrukture());
        //nakon kreiranja svega kaj treba
        for (Ulica ulica : ulice) ulica.generirajOtpadKorisnika();
    }

    private static void kreirajUliceDodajKorisnike(List<String[]> listaUlica) throws NumberFormatException {
        //kreiraj ulice
        //List<Ulica> ulice = new ArrayList<>();
        for (String[] zapisUlica : listaUlica) {
            ulice.add(new Ulica(zapisUlica[0],zapisUlica[1],Integer.parseInt(zapisUlica[2]),Integer.parseInt(zapisUlica[3]),Integer.parseInt(zapisUlica[4]),Integer.parseInt(zapisUlica[5])));
        }
        //dodaj korisnike
        for (Ulica ulica : ulice) {
            ulica.odrediKorisnike();
            ulica.dodijeliSpremnike();
            //System.out.println(ulica.info());
        }
    }

    private static void citajVozila() throws NumberFormatException {
        ReaderFactory rf;
        FileReader fr;
        rf = new ReaderFactory("vozila");
        fr = rf.fileReader();
        List<String[]> listaVozila = fr.getRecords();
        //List<Vozilo> vozila = new ArrayList<>();
        for(String[] zapisVozila : listaVozila){
            vozila.add(new Vozilo(zapisVozila[0],zapisVozila[1],Integer.parseInt(zapisVozila[2]),Integer.parseInt(zapisVozila[3]),Integer.parseInt(zapisVozila[4]),zapisVozila[5]));
        }
    }

    private static void citajSpremnici() throws NumberFormatException {
        ReaderFactory rf;
        FileReader fr;
        rf = new ReaderFactory("spremnici");
        fr = rf.fileReader();
        List<String[]> listaSpremnika = fr.getRecords();
        for(String[] zapisSpremnik:listaSpremnika){
            SpremnikKategorija ks = new SpremnikKategorija(zapisSpremnik[0],Integer.parseInt(zapisSpremnik[1]),Integer.parseInt(zapisSpremnik[2]),Integer.parseInt(zapisSpremnik[3]),Integer.parseInt(zapisSpremnik[4]),Integer.parseInt(zapisSpremnik[5]));
        }
    }

    private static List<String[]> citajUlice() {
        ReaderFactory rf;
        FileReader fr;
        rf = new ReaderFactory("ulice");
        fr = rf.fileReader();
        List<String[]> listaUlica = fr.getRecords();
        return listaUlica;
    }

    public static boolean imaOtpada(List<Ulica> ulice) {
        for(Ulica u:ulice){
            for (Spremnik s: u.spremnici) 
                if(s.kolicinaOtpada>0) return true;
        }
        return false;
    }

    private static void ispisiOtpad() {
        Ispis.getInstance().ispisZaDatoteku("\nIspis otpada po područjima");
        Ispis.getInstance().ispisZaDatoteku("============================");
        for(Podrucje p:Podrucje.svaPodrucja){
            Ispis.getInstance().ispisZaDatoteku(p.getNaziv() + ": " + p.getOtpadStaklo() + "kg stakla, " + p.getOtpadMetal() + "kg metala, " + p.getOtpadPapir() + "kg papira, " + p.getOtpadBio()+"kg bio otpada i " + p.getOtpadMjesano()+"kg mješanog. Ukupno: " + p.getUkupniOtpad() + "kg");
        }
        Ispis.getInstance().ispisZaDatoteku("============================\n");
        for (Ulica ulica : ulice) ulica.odloziOtpad();
    }
    
}