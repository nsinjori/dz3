/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.uzdiz.nsinjori.dz3.FactoryMethod;

import java.util.List;

/**
 *
 * @author Nikola
 */
public interface FileReader {
    public List<String[]> getRecords();
}
