/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.uzdiz.nsinjori.dz3.FactoryMethod;

import org.foi.uzdiz.nsinjori.dz3.Singleton.Ispis;
import org.foi.uzdiz.nsinjori.dz3.Singleton.Parametri;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nikola
 */
public class ReaderVozila implements FileReader{
    @Override
    public List<String[]> getRecords() {
        String fileName = Parametri.getDatotekaVozila();
        List<String[]> listaUlica = new ArrayList<>();
        if (fileName != null){
            try{
                RandomAccessFile file = new RandomAccessFile(fileName, "r");
                String procitaniString;
                file.readLine();
                while((procitaniString = file.readLine())!= null){
                    procitaniString = new String(procitaniString.getBytes("ISO-8859-1"), "UTF-8");
                    if(isOkVozilo(procitaniString)){
                            listaUlica.add(procitaniString.split(";"));
                    }else{
                        Ispis.getInstance().ispis("Nepravilan zapis u datoteci vozila: " + procitaniString);
                }
            }
            }catch(IOException e){

            }
        }
        return listaUlica;
    }
    
    private boolean isOkVozilo(String str){
        String[] parts = str.split(";");
        if(parts.length==6){
            //TODO:
            return true;
        }else return false;
    }
}

