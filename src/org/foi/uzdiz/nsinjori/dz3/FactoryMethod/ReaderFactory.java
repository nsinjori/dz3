/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.uzdiz.nsinjori.dz3.FactoryMethod;

/**
 *
 * @author Nikola
 */
public class ReaderFactory {
    private String datoteka;
    public ReaderFactory(String dat){
        datoteka = dat;
    }
    public FileReader fileReader(){
        if("ulice".equals(datoteka)) return (FileReader) new ReaderUlice();
        if("spremnici".equals(datoteka)) return (FileReader) new ReaderSpremnici();
        if("vozila".equals(datoteka)) return (FileReader) new ReaderVozila();
        if("podrucja".equals(datoteka)) return (FileReader) new ReaderPodrucja();
        if("dispecer".equals(datoteka)) return (FileReader) new ReaderDispecer();
        else return null;
    }
}