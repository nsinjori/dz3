/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.uzdiz.nsinjori.dz3.CoR;

import static org.foi.uzdiz.nsinjori.dz3.Aplikacija.nsinjori_zadaca_3.listaParkiranihVozila;
import static org.foi.uzdiz.nsinjori.dz3.Aplikacija.nsinjori_zadaca_3.listaPokvarenihVozila;
import static org.foi.uzdiz.nsinjori.dz3.Aplikacija.nsinjori_zadaca_3.listaPripremljenihVozila;
import org.foi.uzdiz.nsinjori.dz3.Podaci.Vozilo;
import org.foi.uzdiz.nsinjori.dz3.Singleton.Ispis;

/**
 *
 * @author Nikola
 */
public class isprazniKomanda implements Chain{
    
    private Chain nextInChain;

    @Override
    public void setNextChain(Chain nextChain) {
        this.nextInChain = nextChain;
    }

    @Override
    public void obradiKomandu(Komande request) {
        if(request.getKomanda() == "ISPRAZNI"){
            System.out.println(request.getProcitanaKomanda());
            String[] parts = request.getProcitanaKomanda().split(";");
            if(parts.length==2){
                String[] idjeviVozila = parts[1].split(",");
                for(String id:idjeviVozila){
                    Vozilo v = Vozilo.getByID(id);
                    if(!listaPripremljenihVozila.contains(v)) listaPripremljenihVozila.add(v);
                    if(listaPokvarenihVozila.contains(v)) listaPokvarenihVozila.remove(v);
                    if(listaParkiranihVozila.contains(v)) listaParkiranihVozila.remove(v);
                    v.status = 0;
                    Ispis.getInstance().ispisZaDatoteku("Isprazni vozilo " + v.naziv);
                }
            }else{
                //se fue
            }
        }
        else {
            nextInChain.obradiKomandu(request);
        }
    }
    
}
