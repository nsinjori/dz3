/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.uzdiz.nsinjori.dz3.CoR;

import static org.foi.uzdiz.nsinjori.dz3.Aplikacija.nsinjori_zadaca_3.imaOtpada;
import static org.foi.uzdiz.nsinjori.dz3.Aplikacija.nsinjori_zadaca_3.listaPokvarenihVozila;
import static org.foi.uzdiz.nsinjori.dz3.Aplikacija.nsinjori_zadaca_3.listaPripremljenihVozila;
import static org.foi.uzdiz.nsinjori.dz3.Aplikacija.nsinjori_zadaca_3.ulice;
import org.foi.uzdiz.nsinjori.dz3.Podaci.Vozilo;
import org.foi.uzdiz.nsinjori.dz3.Singleton.Ispis;

/**
 *
 * @author Nikola
 */
public class kvarKomanda implements Chain{
    
    private Chain nextInChain;

    @Override
    public void setNextChain(Chain nextChain) {
        this.nextInChain = nextChain;
    }

    @Override
    public void obradiKomandu(Komande request) {
        if(request.getKomanda() == "KVAR"){
            System.out.println(request.getProcitanaKomanda());
            String[] parts = request.getProcitanaKomanda().split(";");
            if(parts.length==2){
                String[] idjeviVozila = parts[1].split(",");
                for(String id:idjeviVozila){
                    Vozilo v = Vozilo.getByID(id);
                    listaPokvarenihVozila.add(v);
                    listaPripremljenihVozila.remove(v);
                    Ispis.getInstance().ispisZaDatoteku("Kvar vozila " + v.naziv);
                }
            }else{
                //se fue
            }
        }
        else {
            nextInChain.obradiKomandu(request);
        }
    }
    
}
