/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.uzdiz.nsinjori.dz3.CoR;

import static org.foi.uzdiz.nsinjori.dz3.Aplikacija.nsinjori_zadaca_3.listaParkiranihVozila;
import static org.foi.uzdiz.nsinjori.dz3.Aplikacija.nsinjori_zadaca_3.listaPokvarenihVozila;
import static org.foi.uzdiz.nsinjori.dz3.Aplikacija.nsinjori_zadaca_3.listaPripremljenihVozila;
import org.foi.uzdiz.nsinjori.dz3.Podaci.Vozilo;
import org.foi.uzdiz.nsinjori.dz3.Singleton.Ispis;

/**
 *
 * @author Nikola
 */
public class statusKomanda implements Chain{
    private Chain nextInChain;

    @Override
    public void setNextChain(Chain nextChain) {
        this.nextInChain = nextChain;
    }

    @Override
    public void obradiKomandu(Komande request) {
        if(request.getKomanda() == "STATUS"){
            System.out.println(request.getProcitanaKomanda());
            String[] parts = request.getProcitanaKomanda().split(";");
            Ispis.getInstance().ispisZaDatoteku("---------------------");
            Ispis.getInstance().ispisZaDatoteku("Parking\n");
            for(Vozilo v:listaParkiranihVozila) Ispis.getInstance().ispisZaDatoteku(v.naziv + "[" + v.id+"]" + "\n");
            Ispis.getInstance().ispisZaDatoteku("---------------------");
            Ispis.getInstance().ispisZaDatoteku("Pripremljena\n");
            for(Vozilo v:listaPripremljenihVozila) Ispis.getInstance().ispisZaDatoteku(v.naziv + "[" + v.id+"]" + "\n");
            Ispis.getInstance().ispisZaDatoteku("---------------------");
            Ispis.getInstance().ispisZaDatoteku("Pokvarena\n");
            for(Vozilo v:listaPokvarenihVozila) Ispis.getInstance().ispisZaDatoteku(v.naziv + "[" + v.id+"]" + "\n");
            Ispis.getInstance().ispisZaDatoteku("---------------------");
        }
        else {
            nextInChain.obradiKomandu(request);
        }
    }
    
}
