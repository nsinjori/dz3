/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.uzdiz.nsinjori.dz3.CoR;

import static org.foi.uzdiz.nsinjori.dz3.Aplikacija.nsinjori_zadaca_3.listaParkiranihVozila;
import static org.foi.uzdiz.nsinjori.dz3.Aplikacija.nsinjori_zadaca_3.listaPripremljenihVozila;
import org.foi.uzdiz.nsinjori.dz3.Podaci.Vozilo;
import org.foi.uzdiz.nsinjori.dz3.Singleton.Ispis;

/**
 *
 * @author Nikola
 */
public class pripremiKomanda implements Chain{
    
    
    private Chain nextInChain;

    @Override
    public void setNextChain(Chain nextChain) {
        this.nextInChain = nextChain;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void obradiKomandu(Komande request) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if(request.getKomanda() == "PRIPREMI"){
        System.out.println(request.getProcitanaKomanda());
            String[] parts = request.getProcitanaKomanda().split(";");
            if(parts.length==2){
                String[] idjeviVozila = parts[1].split(",");
                for(String id:idjeviVozila){
                    Vozilo v = Vozilo.getByID(id);
                    listaPripremljenihVozila.add(v);
                    listaParkiranihVozila.remove(v);
                    Ispis.getInstance().ispisZaDatoteku("Priprema vozila " + v.naziv);
                    //listaPripremljenihVozila.forEach(System.out::println);
                }
            }else{
                //nesto
            }
        }
        else {
            nextInChain.obradiKomandu(request);
        }

    }
    
}
