/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.uzdiz.nsinjori.dz3.CoR;

import static org.foi.uzdiz.nsinjori.dz3.Aplikacija.nsinjori_zadaca_3.imaOtpada;
import static org.foi.uzdiz.nsinjori.dz3.Aplikacija.nsinjori_zadaca_3.listaParkiranihVozila;
import static org.foi.uzdiz.nsinjori.dz3.Aplikacija.nsinjori_zadaca_3.listaPripremljenihVozila;
import static org.foi.uzdiz.nsinjori.dz3.Aplikacija.nsinjori_zadaca_3.ulice;
import org.foi.uzdiz.nsinjori.dz3.Podaci.Vozilo;

/**
 *
 * @author Nikola
 */
public class kreniKomanda implements Chain{
    
    private Chain nextInChain;

    @Override
    public void setNextChain(Chain nextChain) {
        
        this.nextInChain = nextChain;
    }

    @Override
    public void obradiKomandu(Komande request) {
        //System.out.println(request.getKomanda());
        
        if(request.getKomanda() == "KRENI"){
            System.out.println(request.getProcitanaKomanda());
            String[] parts = request.getProcitanaKomanda().split(" ");
            if(parts.length==2){
                int brojCiklusa = Integer.parseInt(parts[1]);
                //int brojCiklusa = Integer.parseInt(parts[1].substring(0,parts[1].length()-1));

                for(int i=0;i<brojCiklusa;i++){
                    for(Vozilo v: listaPripremljenihVozila){
                        v.preuzmiOtpad(ulice);
                    }
                }
            }else{
                while(imaOtpada(ulice)){
                    //preuzmi otpad
                    for(Vozilo v: listaPripremljenihVozila){
                        v.preuzmiOtpad(ulice);
                    }
                }
            }
        }
        else {
            nextInChain.obradiKomandu(request);
        }
    }
    
}
