/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.uzdiz.nsinjori.dz3.CoR;

/**
 *
 * @author Nikola
 */
public class Komande {
    
    private String procitanaKomanda;
    
    private String komanda;

    public Komande(String procitanaKomanda) {
        this.procitanaKomanda = procitanaKomanda;
    }

    public String getProcitanaKomanda() {
        return procitanaKomanda;
    }

    public String getKomanda() {
        String izvrsiKomandu = null;
        if (procitanaKomanda.contains("PRIPREMI")){
            izvrsiKomandu = "PRIPREMI";
        }
        if (procitanaKomanda.contains("ISPRAZNI")){
            izvrsiKomandu =  "ISPRAZNI";
        }
        if (procitanaKomanda.contains("KRENI")){
            izvrsiKomandu =  "KRENI";
        }
        if (procitanaKomanda.contains("KVAR")){
            izvrsiKomandu =  "KVAR";
        }
        if (procitanaKomanda.contains("STATUS")){
            izvrsiKomandu =  "STATUS";
        }  
        if (procitanaKomanda.contains("KONTROLA")){
            izvrsiKomandu =  "KONTROLA";
        } 
        return izvrsiKomandu;
    }


    
    
}
