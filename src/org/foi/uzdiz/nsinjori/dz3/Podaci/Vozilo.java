/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.uzdiz.nsinjori.dz3.Podaci;

import static org.foi.uzdiz.nsinjori.dz3.Aplikacija.nsinjori_zadaca_3.brojac;
import org.foi.uzdiz.nsinjori.dz3.Singleton.Ispis;
import org.foi.uzdiz.nsinjori.dz3.Singleton.Parametri;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nikola
 */
public class Vozilo {

    public String id;
    public String naziv;
    int tip;
    int vrsta;
    int nosivost;
    String vozaci;
    public int trenutnaKolicina;
    public int status;
    int ciklusiOdvoza;
    String vrstaString;
    public static List<Vozilo> svaVozila = new ArrayList<>();

    public Vozilo(String id, String naziv, int tip, int vrsta, int nosivost, String vozaci) {
        this.id = id;
        this.naziv = naziv;
        this.tip = tip;
        this.vrsta = vrsta;
        this.nosivost = nosivost;
        this.vozaci = vozaci;
        this.trenutnaKolicina = 0;
        this.status = 1;
        this.ciklusiOdvoza = 0;
        this.vrstaString = mapirajVrstu(vrsta);
        svaVozila.add(this);
    }

    public void preuzmiOtpad(List<Ulica> ulice) {
        if (status == 1) {
            for (Ulica u : ulice) {
                for (Spremnik s : u.spremnici) {
                    if (s.naziv.equals(vrstaString) && s.kolicinaOtpada > 0) {
                        if ((trenutnaKolicina + s.kolicinaOtpada) < nosivost) {
                            trenutnaKolicina += s.kolicinaOtpada;

                            Ispis.getInstance().ispisZaDatoteku(brojac + ". Vozilo " + naziv + " je preuzelo " + s.kolicinaOtpada + "kg otpada iz spremnika " + s.shortInfo() + ". => " + trenutnaKolicina + "/" + nosivost);
                            s.kolicinaOtpada = 0;
                        } else {
                            status = 0;
                            Ispis.getInstance().ispisZaDatoteku(brojac + ". Vozilo " + naziv + " je napunjeno i odvozi otpad na odlagalište!");
                        }
                        brojac++;
                        return;
                    }
                }
            }
        } else {
            ciklusiOdvoza++;
            if (ciklusiOdvoza >= Parametri.getBrojRadnihCiklusaZaOdvoz()) {
                ciklusiOdvoza = 0;
                status = 1;
                trenutnaKolicina = 0;
                Ispis.getInstance().ispisZaDatoteku(brojac + ". Vozilo " + naziv + " je obavilo odvoz otpada na odlagalište!");
            } else {
                Ispis.getInstance().ispisZaDatoteku(brojac + ". Vozilo " + naziv + " odvozi otpad na odlagalište!");
            }
            brojac++;
        }
    }

    private String mapirajVrstu(int vrsta) {
        if (vrsta == 0) {
            return "staklo";
        }
        if (vrsta == 1) {
            return "papir";
        }
        if (vrsta == 2) {
            return "metal";
        }
        if (vrsta == 3) {
            return "bio";
        }
        if (vrsta == 4) {
            return "mješano";
        } else {
            return "";
        }
    }

    public void iprazniKamion() {
        Ispis.getInstance().ispisZaDatoteku("Vozilo " + naziv + " odvozi otpad na odlagalište!");
    }

    public static Vozilo getByID(String id) {
        for (Vozilo v : svaVozila) {
            if (v.id.equals(id)) {
                return v;
            }
        }
        return null;
    }
}
