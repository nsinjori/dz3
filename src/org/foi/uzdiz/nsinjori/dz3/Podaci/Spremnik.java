/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.uzdiz.nsinjori.dz3.Podaci;

import org.foi.uzdiz.nsinjori.dz3.Singleton.Ispis;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nikola
 */
public class Spremnik {
    int id;
    String naziv;
    int tip;
    int nosivost;
    int naMaleKorisnike;
    int naSrednjeKorisnike;
    int naVelikeKorisnike;
    public float kolicinaOtpada;
    public static List<Spremnik> listaSpremnika = new ArrayList<>();
    public List<Korisnik> korisniciSpremnika;
    public Spremnik(String naziv, int tip, int naMale, int naSrednje, int naVelike, int nosivost) {
        this.naziv = naziv;
        this.tip = tip;
        this.nosivost = nosivost;
        this.naMaleKorisnike = naMale;
        this.naSrednjeKorisnike = naSrednje;
        this.naVelikeKorisnike = naVelike;
        korisniciSpremnika = new ArrayList<>();
        kolicinaOtpada = 0;
        dodajID();
        listaSpremnika.add(this);
    }
    private void dodajID(){
        int noviID = 1000;
        while(postojiID(noviID)){
            noviID++;
        }
        this.id = noviID;
    }
    private boolean postojiID(int nid){
        for(Spremnik s: listaSpremnika) if(s.id==nid)return true;
        return false;
    }

    boolean mozePrimitKorisnika(int i) {
        if(i==1){
            if(korisniciSpremnika.size()<naMaleKorisnike)return true;
            else return false;
        }
        if(i==2){
            if(korisniciSpremnika.size()<naSrednjeKorisnike)return true;
            else return false;
        }
        if(i==3){
            if(korisniciSpremnika.size()<naVelikeKorisnike)return true;
            else return false;
        }
        return false;
    }

    float dodajOtpad(Korisnik k, float otpad) {
        int kid = k.ajdiKorisnika;
        if((kolicinaOtpada+otpad)<=nosivost){
            kolicinaOtpada = kolicinaOtpada+otpad;
            Ispis.getInstance().ispisZaDatoteku("Korisnik ("+kid+") je u spremnik " + naziv + " (" + id + ") bacio " + otpad + "kg otpada");
            return otpad;
        }else if(kolicinaOtpada<nosivost){
            float diff = nosivost-kolicinaOtpada;
            kolicinaOtpada = nosivost;
            float preostali = otpad-diff;
            Ispis.getInstance().ispisZaDatoteku("Korisnik ("+kid+") je u spremnik " + naziv + " (" + id + ") bacio " + diff + "kg otpada i napunio kontenjer. Ostalo mu je "+preostali+"kg. ");
            return diff;
        }else{
            Ispis.getInstance().ispisZaDatoteku("Korisnik ("+kid+") ne može baciti otpad jer je spremnik " + naziv + " (" + id + ") puni!");
            return 0;
        }
    }
    public Spremnik klonirajSpremnik(){
        return new Spremnik(naziv, tip, naMaleKorisnike, naSrednjeKorisnike, naVelikeKorisnike, nosivost);
    }
    String shortInfo() {
        return naziv+" [" + id + "]";
    }

}
