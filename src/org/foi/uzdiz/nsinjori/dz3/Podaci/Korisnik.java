/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.uzdiz.nsinjori.dz3.Podaci;

import org.foi.uzdiz.nsinjori.dz3.Singleton.Ispis;
import org.foi.uzdiz.nsinjori.dz3.Singleton.Parametri;
import org.foi.uzdiz.nsinjori.dz3.Singleton.RandomGenerator;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nikola
 */
public class Korisnik {
    int ajdiKorisnika;
    int tipKorisnika;
    float otpadStaklo;
    float otpadPapir;
    float otpadMetal;
    float otpadBio;
    float otpadMjesano;
    public static List<Korisnik> listaKorisnika = new ArrayList<>();
    public List<Spremnik> dodijeljeniSpremnici;
    public Korisnik(int k){
        tipKorisnika = k;
        dodajID();
        dodijeljeniSpremnici = new ArrayList<>();
        listaKorisnika.add(this);
    }
    private void dodajID(){
        int noviID = 1;
        while(postojiID(noviID)){
            noviID++;
        }
        this.ajdiKorisnika = noviID;
    }
    private boolean postojiID(int nid){
        for(Korisnik k:listaKorisnika) if(k.ajdiKorisnika==nid) return true;
        return false;
    }
    public String info(){
        String info = "Korisnik " + ajdiKorisnika + "(" + tipKorisnika + ")\n";
        info += "Spremnici: ";
        for(Spremnik s: dodijeljeniSpremnici){
            info+= s.naziv + " " + s.id + " | ";
        }
        return info;
    }
    public void generirajOtpad(){
        if(tipKorisnika==1){
            int min = Parametri.getMaliMin();
            otpadStaklo = RandomGenerator.getRandomGeneratorInstance().dajSlucajniBrojDecimale((float)((min/100.0)*Parametri.getMaliStaklo()), Parametri.getMaliStaklo());
            otpadPapir = RandomGenerator.getRandomGeneratorInstance().dajSlucajniBrojDecimale((float)((min/100.0)*Parametri.getMaliPapir()), Parametri.getMaliPapir());
            otpadMetal = RandomGenerator.getRandomGeneratorInstance().dajSlucajniBrojDecimale((float)((min/100.0)*Parametri.getMaliMetal()), Parametri.getMaliMetal());
            otpadBio = RandomGenerator.getRandomGeneratorInstance().dajSlucajniBrojDecimale((float)((min/100.0)*Parametri.getMaliBio()), Parametri.getMaliBio());
            otpadMjesano = RandomGenerator.getRandomGeneratorInstance().dajSlucajniBrojDecimale((float)((min/100.0)*Parametri.getMaliMjesano()), Parametri.getMaliMjesano());
        }
        if(tipKorisnika==2){
            int min = Parametri.getSrednjiMin();
            otpadStaklo = RandomGenerator.getRandomGeneratorInstance().dajSlucajniBrojDecimale((float)((min/100.0)*Parametri.getSrednjiStaklo()), Parametri.getSrednjiStaklo());
            otpadPapir = RandomGenerator.getRandomGeneratorInstance().dajSlucajniBrojDecimale((float)((min/100.0)*Parametri.getSrednjiPapir()), Parametri.getSrednjiPapir());
            otpadMetal = RandomGenerator.getRandomGeneratorInstance().dajSlucajniBrojDecimale((float)((min/100.0)*Parametri.getSrednjiMetal()), Parametri.getSrednjiMetal());
            otpadBio = RandomGenerator.getRandomGeneratorInstance().dajSlucajniBrojDecimale((float)((min/100.0)*Parametri.getSrednjiBio()), Parametri.getSrednjiBio());
            otpadMjesano = RandomGenerator.getRandomGeneratorInstance().dajSlucajniBrojDecimale((float)((min/100.0)*Parametri.getSrednjiMjesano()), Parametri.getSrednjiMjesano());
        }
        if(tipKorisnika==3){
            int min = Parametri.getVelikiMin();
            otpadStaklo = RandomGenerator.getRandomGeneratorInstance().dajSlucajniBrojDecimale((float)((min/100.0)*Parametri.getVelikiStaklo()), Parametri.getVelikiStaklo());
            otpadPapir = RandomGenerator.getRandomGeneratorInstance().dajSlucajniBrojDecimale((float)((min/100.0)*Parametri.getVelikiPapir()), Parametri.getVelikiPapir());
            otpadMetal = RandomGenerator.getRandomGeneratorInstance().dajSlucajniBrojDecimale((float)((min/100.0)*Parametri.getVelikiMetal()), Parametri.getVelikiMetal());
            otpadBio = RandomGenerator.getRandomGeneratorInstance().dajSlucajniBrojDecimale((float)((min/100.0)*Parametri.getVelikiBio()), Parametri.getVelikiBio());
            otpadMjesano = RandomGenerator.getRandomGeneratorInstance().dajSlucajniBrojDecimale((float)((min/100.0)*Parametri.getVelikiMjesano()), Parametri.getVelikiMjesano());
        }
        Ispis.getInstance().ispisZaDatoteku("Korisnik (" + ajdiKorisnika + ")["+tipKorisnika+"] : Staklo - " + otpadStaklo + ", Papir - " + otpadPapir + ", Metal - " + otpadMetal + ", Bio - " + otpadBio + ", Mješano - " + otpadMjesano);
    }
    public void baciOtpad(){
        for(Spremnik s:dodijeljeniSpremnici){
            if(s.naziv.equals("staklo")){
                otpadStaklo = otpadStaklo - s.dodajOtpad(this,otpadStaklo);
            }
            if(s.naziv.equals("papir")){
                otpadPapir = otpadPapir -s.dodajOtpad(this,otpadPapir);
            }
            if(s.naziv.equals("metal")){
                otpadMetal = otpadMetal -s.dodajOtpad(this,otpadMetal);
            }
            if(s.naziv.equals("bio")){
                otpadBio = otpadBio -s.dodajOtpad(this,otpadBio);
            }
            if(s.naziv.equals("mješano")){
                otpadMjesano = otpadMjesano - s.dodajOtpad(this,otpadMjesano);
            }
            
        }
    }
    
    public float getOtpadStaklo() {
        return otpadStaklo;
    }

    public float getOtpadPapir() {
        return otpadPapir;
    }

    public float getOtpadMetal() {
        return otpadMetal;
    }

    public float getOtpadBio() {
        return otpadBio;
    }

    public float getOtpadMjesano() {
        return otpadMjesano;
    }
}
