/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.uzdiz.nsinjori.dz3.Podaci;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nikola
 */
public class SpremnikKategorija {
    String naziv;
    int tip;
    int nosivost;
    int naMale;
    int naSrednje;
    int naVelike;
    public static List<SpremnikKategorija> listaKategorijaSpremnika = new ArrayList<>();

    public SpremnikKategorija(String naziv, int tip, int naMale, int naSrednje, int naVelike,int nosivost) {
        this.naziv = naziv;
        this.tip = tip;
        this.nosivost = nosivost;
        this.naMale = naMale;
        this.naSrednje = naSrednje;
        this.naVelike = naVelike;
        listaKategorijaSpremnika.add(this);
    }
}
