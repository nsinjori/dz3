/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.uzdiz.nsinjori.dz3.Podaci;

import org.foi.uzdiz.nsinjori.dz3.Singleton.Ispis;
import java.util.ArrayList;
import java.util.List;
import static org.foi.uzdiz.nsinjori.dz3.Aplikacija.nsinjori_zadaca_3.brojac;

/**
 *
 * @author Nikola
 */
public class Ulica {
    public String id;
    public String naziv;
    int brojMjesta;
    int udioMali;
    int udioSrednji;
    int udioVeliki;
    int brojMalih;
    int brojSrednjih;
    int brojVelikih;
    
    float otpadStaklo;
    float otpadPapir;
    float otpadMetal;
    float otpadBio;
    float otpadMjesano;
    
    public String idPodrucja;
    
    public List<Korisnik> stanovnici;
    public List<Spremnik> spremnici;
    public static List<Ulica> sveUlice = new ArrayList<>();
    public Ulica(String id,String naziv, int brojMjesta, int udioMali, int udioSrednji, int udioVeliki) {
        this.id = id;
        this.naziv = naziv;
        this.brojMjesta = brojMjesta;
        this.udioMali = udioMali;
        this.udioSrednji = udioSrednji;
        this.udioVeliki = udioVeliki;
        stanovnici = new ArrayList<>();
        spremnici = new ArrayList<>();
        this.idPodrucja = "-1"; //ili mozda neki drugi nacin? npr bool flag
        sveUlice.add(this);
    }

    public void odrediKorisnike() {
        brojMalih = (int)(brojMjesta*(udioMali/100.0));
        brojSrednjih = (int)(brojMjesta*(udioSrednji/100.0));
        brojVelikih = (int)(brojMjesta*(udioVeliki/100.0));
        int ukupno = brojMalih+brojVelikih+brojSrednjih;
        if(ukupno!=brojMjesta){
            if(ukupno<brojMjesta){//ako postotak zezne stvari da ne pase
                while (ukupno<brojMjesta){
                    brojMalih++;
                    ukupno++;
                }
            }else{
                while (ukupno>brojMjesta){
                    brojMalih--;
                    ukupno--;
                }
            }
        }
        for (int i = 0; i < brojMalih; i++) {
            stanovnici.add(new Korisnik(1));
        }
        for (int i = 0; i < brojSrednjih; i++) {
            stanovnici.add(new Korisnik(2));
        }
        for (int i = 0; i < brojVelikih; i++) {
            stanovnici.add(new Korisnik(3));
        }
    }
    public void dodijeliSpremnike(){
        for(SpremnikKategorija ks : SpremnikKategorija.listaKategorijaSpremnika){
            if(ks.naMale>0&&brojMalih>0){
                dodajMaliKorisniciSpremnik(ks);
            }
            if(ks.naSrednje>0&&brojSrednjih>0){
                dodajSrednjiKorisniciSpremnik(ks);
            }
            if(ks.naVelike>0&&brojVelikih>0){
                dodajVelikiKorisniciSpremnik(ks);
            }
        }
    }

    private void dodajVelikiKorisniciSpremnik(SpremnikKategorija sk) {
        //dodavanje spremnika velikim korisnicima
        Spremnik noviSpremnik = new Spremnik(sk.naziv, sk.tip, sk.naMale, sk.naSrednje, sk.naVelike, sk.nosivost);
        spremnici.add(noviSpremnik);
        for(Korisnik k: stanovnici){
            if(k.tipKorisnika==3){
                if(noviSpremnik.mozePrimitKorisnika(k.tipKorisnika)){
                    k.dodijeljeniSpremnici.add(noviSpremnik);
                    noviSpremnik.korisniciSpremnika.add(k);
                }else{
                    noviSpremnik = new Spremnik(sk.naziv, sk.tip, sk.naMale, sk.naSrednje, sk.naVelike, sk.nosivost);
                    spremnici.add(noviSpremnik);
                    k.dodijeljeniSpremnici.add(noviSpremnik);
                    noviSpremnik.korisniciSpremnika.add(k);
                }
            }
        }
    }

    private void dodajSrednjiKorisniciSpremnik(SpremnikKategorija sk) {
        //dodavanje spremnika srednjim korisnicima
        Spremnik noviSpremnik = new Spremnik(sk.naziv, sk.tip, sk.naMale, sk.naSrednje, sk.naVelike, sk.nosivost);
        spremnici.add(noviSpremnik);
        for(Korisnik k: stanovnici){
            if(k.tipKorisnika==2){
                if(noviSpremnik.mozePrimitKorisnika(k.tipKorisnika)){
                    k.dodijeljeniSpremnici.add(noviSpremnik);
                    noviSpremnik.korisniciSpremnika.add(k);
                }else{
                    noviSpremnik = noviSpremnik.klonirajSpremnik();
                    spremnici.add(noviSpremnik);
                    k.dodijeljeniSpremnici.add(noviSpremnik);
                    noviSpremnik.korisniciSpremnika.add(k);
                }
            }
        }
    }

    private void dodajMaliKorisniciSpremnik(SpremnikKategorija sk) {
        //dodavanje spremnika malim korisnicima
        Spremnik noviSpremnik = new Spremnik(sk.naziv, sk.tip, sk.naMale, sk.naSrednje, sk.naVelike, sk.nosivost);
        spremnici.add(noviSpremnik);
        for(Korisnik k: stanovnici){
            if(k.tipKorisnika==1){
                if(noviSpremnik.mozePrimitKorisnika(k.tipKorisnika)){
                    k.dodijeljeniSpremnici.add(noviSpremnik);
                    noviSpremnik.korisniciSpremnika.add(k);
                }else{
                    noviSpremnik = noviSpremnik.klonirajSpremnik();
                    spremnici.add(noviSpremnik);
                    k.dodijeljeniSpremnici.add(noviSpremnik);
                    noviSpremnik.korisniciSpremnika.add(k);
                }
            }
        }
    }
    public void generirajOtpadKorisnika(){
        Ispis.getInstance().ispisZaDatoteku("***generiranje otpada***"+naziv + "***");
        for(Korisnik k: stanovnici){
            k.generirajOtpad();
        }
        
    }
    public void odloziOtpad(){
        Ispis.getInstance().ispisZaDatoteku("***odlaganje otpada***"+naziv+"***");
        for(Korisnik k: stanovnici){
            k.baciOtpad();
        }
        for(Spremnik s: spremnici){
            if(s.naziv.equals("staklo")) otpadStaklo+=s.kolicinaOtpada;
            if(s.naziv.equals("papir")) otpadPapir+=s.kolicinaOtpada;
            if(s.naziv.equals("metal")) otpadMetal+=s.kolicinaOtpada;
            if(s.naziv.equals("bio")) otpadBio+=s.kolicinaOtpada;
            if(s.naziv.equals("mješano")) otpadMjesano+=s.kolicinaOtpada;
        }
        Ispis.getInstance().ispisZaDatoteku("\nOtpad u ulici " + naziv + "\n");
        Ispis.getInstance().ispisZaDatoteku("Staklo: " + otpadStaklo);
        Ispis.getInstance().ispisZaDatoteku("Papir: " + otpadPapir);
        Ispis.getInstance().ispisZaDatoteku("Metal: " + otpadMetal);
        Ispis.getInstance().ispisZaDatoteku("Bio: " + otpadBio);
        Ispis.getInstance().ispisZaDatoteku("Mješano: " + otpadMjesano);
        Ispis.getInstance().ispisZaDatoteku("\n\n");
    }
    public String info(){
        String info = naziv + "\nStanovnici: \n";
        for(Korisnik k:stanovnici) info = info + k.ajdiKorisnika + ":  "+ k.tipKorisnika + "\n";
        return info+"\n";
    }
}