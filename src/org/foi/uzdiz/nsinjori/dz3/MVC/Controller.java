/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.uzdiz.nsinjori.dz3.MVC;

import org.foi.uzdiz.nsinjori.dz3.Singleton.Ispis;

/**
 *
 * @author Nikola
 */
public class Controller {
    
    private final View prikaz;

    public Controller(View v) {
        this.prikaz = v;
    }

    public void obradiNaredbu(String readLine) {
        String output = "";

        if (readLine.contains("PRIPREMI")) {
            prikaz.ispisPodataka("\nZnam ovo resiti");
        } else if (readLine.contains("ISPRAZNI")) {
            prikaz.ispisPodataka("\nZnam ovo resiti");
        } else if (readLine.contains("KRENI")) {
            prikaz.ispisPodataka("\nZnam ovo resiti");
        } else if (readLine.contains("KVAR")) {
            prikaz.ispisPodataka("\nZnam ovo resiti");
        } else if (readLine.contains("STATUS")) {
            prikaz.ispisPodataka("\nZnam ovo resiti");
        } else if (readLine.contains("KONTROLA")) {
            prikaz.ispisPodataka("\nZnam ovo resiti");
        } else if (readLine.contains("OBRADI")) {
            prikaz.ispisPodataka("\nNe znam ovo resiti");
        } else if (readLine.contains("GODIŠNJI ODMOR")) {
            prikaz.ispisPodataka("\nNe znam ovo resiti");
        } else if (readLine.contains("BOLOVANJE")) {
            prikaz.ispisPodataka("\nNe znam ovo resiti");
        } else if (readLine.contains("OTKAZ")) {
            prikaz.ispisPodataka("\nNe znam ovo resiti");
        } else if (readLine.contains("PREUZMI")) {
            prikaz.ispisPodataka("\nNe znam ovo resiti");
        } else if (readLine.contains("NOVI")) {
            prikaz.ispisPodataka("\nNe znam ovo resiti");
        } else if (readLine.contains("VOZAČI")) {
            prikaz.ispisPodataka("\nNe znam ovo resiti");
        } else if (readLine.contains("IZLAZ")) {
            prikaz.ispisPodataka("\nNe znam ovo resiti");
        } else {
            prikaz.ispisPodataka("\nNepostojeca komada :(");
            //nepostojeca komanda
        };
        
        prikaz.omoguciUnosKomandi();
    }

    

    void pokreni() {
        prikaz.omoguciUnosKomandi();
    }

}
