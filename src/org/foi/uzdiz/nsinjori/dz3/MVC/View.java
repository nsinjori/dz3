/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.uzdiz.nsinjori.dz3.MVC;

import java.util.Scanner;

/**
 *
 * @author Nikola
 */
public class View {
    //private int brg, brd, pocetak_gornji, pocetak_donji, kraj_gornji, kraj_donji, trenutni_gornji, trenutni_donji, sirina;
    
    public static final String ANSI_ESC = "\033[";
    int br;
    int bs;
    int brd;
    int brg;
    private final int brojLinijaZaIspis;
    private int trenutnaLinijaIspisa;
    private int trenutnaLinijaKomandi;
    String[] komande;
    Scanner input = new Scanner(System.in);
    private Controller kontroler;

    public View(int brg, int brd) {
        this.br = brd+brg;
        this.bs = 80;
        this.brd = brd;
        this.brg = brg;
        this.brojLinijaZaIspis = brg;
        trenutnaLinijaIspisa = 1;
        trenutnaLinijaKomandi = brojLinijaZaIspis+2;
        komande = new String[brd+1];
        pocetniPrikaz();
    }

    void pocetniPrikaz() {
        System.out.print(ANSI_ESC + "2J");
        obrisiEkranZaIspis();
        for (int j=1; j < bs; j++) {
            prikazi(brojLinijaZaIspis+1,j,"-");
        }
    }
    private void postavi(int x, int y) {
        System.out.print(ANSI_ESC + x + ";" + y + "f");
    }
    void prikazi(int x, int y, String tekst) {
        postavi(x, y);
        System.out.print(tekst);
    }
    private void obrisiLinju(int x){
            postavi(x, 0);
            System.out.print(ANSI_ESC + "2K");
    }
    private void obrisiEkranZaIspis(){
        for (int i = 1; i <= brojLinijaZaIspis; i++) {
            obrisiLinju(i);
        }
    }
    public void ispisPodataka(String tekstc){
        String[] linije = tekstc.split("\n");
        for(String tekst:linije){
            while(tekst.length() > bs) {
                if(trenutnaLinijaIspisa>brojLinijaZaIspis){
                    napunjenEkran();
                }
                prikazi(trenutnaLinijaIspisa++, 1, tekst.substring(0, Math.min(tekst.length(), bs)));
                tekst = tekst.substring(Math.min(tekst.length(), bs));
            }
            if(trenutnaLinijaIspisa>brojLinijaZaIspis){
                napunjenEkran();
            }
            prikazi(trenutnaLinijaIspisa++, 1, tekst);
        }
        //omoguciUnosKomandi();
    }
    private void napunjenEkran() {
        String l = "i";
        while(!"n".equals(l)&&!"N".equals(l)){
            obrisiEkranZaKomande();
            prikazi(trenutnaLinijaKomandi,0,"n/N za nastavak: ");
            l = System.console().readLine();               
        }
        obrisiEkranZaIspis();
        trenutnaLinijaIspisa = 1;
        obrisiEkranZaKomande();
    }
    private void obrisiEkranZaKomande() {
        for (int i = brojLinijaZaIspis+2; i <= br+2; i++) {
            obrisiLinju(i);
        }
        trenutnaLinijaKomandi=brojLinijaZaIspis+2;
    }
    void omoguciUnosKomandi(){
        if(trenutnaLinijaKomandi==br+2||(trenutnaLinijaKomandi==brojLinijaZaIspis+2&&komande.length!=0)){
            for (int i = 0; i<komande.length-1;i++) {
                if(komande[i+1]!=null)komande[i]=komande[i+1];
                komande[i+1]=null;
            }
        }
        obrisiEkranZaKomande();
        for (String komanda : komande) {
            if(komanda!=null)
                prikazi(trenutnaLinijaKomandi++,0,komanda);
        }
        prikazi(trenutnaLinijaKomandi++,0,">: ");
        String readLine ="";
        while(!"I".equals(readLine)&&!readLine.equals("i")){
            readLine = System.console().readLine();
            obradiNaredbu(readLine);
        }       
    }
    private void obradiNaredbu(String readLine) {
        komande[trenutnaLinijaKomandi-3-brojLinijaZaIspis] = ">: "+readLine;
        if(readLine.length()<1) readLine="-";
        //obradi naredbu kontroler
        kontroler.obradiNaredbu(readLine);
    }
    void clearScreen(){
        for (int i = 1; i <= br+3; i++) {
            obrisiLinju(i);
        }
        postavi(1, 1);
    }

    void dodajKontroler(Controller k) {
        this.kontroler = k;
    }
    
}
