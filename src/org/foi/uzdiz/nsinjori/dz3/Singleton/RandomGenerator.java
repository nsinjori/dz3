/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.uzdiz.nsinjori.dz3.Singleton;

import java.util.Random;

/**
 *
 * @author Nikola
 */
public class RandomGenerator {
    private static RandomGenerator instance;
    static Random slucajniBrojcek;
    
    private RandomGenerator(){
    }
    public static RandomGenerator getRandomGeneratorInstance(){
        if(instance == null){
            instance = new RandomGenerator();
            int sjemeGeneratora = Parametri.getSjemeGeneratora();
            slucajniBrojcek = new Random(sjemeGeneratora);
        }
        return instance;
    }
    public int dajSlucajniBroj(int odBroja, int doBroja){
        int slucajniBroj = slucajniBrojcek.nextInt(doBroja - odBroja) + odBroja;
        return slucajniBroj;            
    }
    public long dajSlucajniBroj(long odBroja, long doBroja){
        long slucajniBroj = odBroja+((long)(slucajniBrojcek.nextDouble()*(doBroja-odBroja)));
        return slucajniBroj;
    }
    public float dajSlucajniBrojDecimale(float odBroja, float doBroja){
        int brojDecimala = Parametri.getBrojDecimala();
        float slucajniBroj = slucajniBrojcek.nextFloat() * (doBroja - odBroja) + odBroja;
        int num = (int)(slucajniBroj*100);
        
        return (float)(num/100.0);
    }
    
}
