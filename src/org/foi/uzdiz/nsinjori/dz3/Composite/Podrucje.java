/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.uzdiz.nsinjori.dz3.Composite;

import org.foi.uzdiz.nsinjori.dz3.Podaci.Ulica;
import org.foi.uzdiz.nsinjori.dz3.Singleton.Ispis;
import java.util.ArrayList;
import java.util.List;
import org.foi.uzdiz.nsinjori.dz3.Podaci.Korisnik;

/**
 *
 * @author Nikola
 */
public class Podrucje {
    String id;
    String naziv;
    String dijelovi;
    String idNadPodrucja;
    List<Podrucje> podPodrucja;
    List<Ulica> uliceZaPodrucje;
    public static List<Podrucje> svaPodrucja = new ArrayList<>();

    public Podrucje(String id, String naziv, String dijelovi) {
        this.id = id;
        this.naziv = naziv;
        this.dijelovi = dijelovi;
        this.podPodrucja = new ArrayList<>();
        this.uliceZaPodrucje = new ArrayList<>();
        this.idNadPodrucja = "-1";
        svaPodrucja.add(this);
    }
    public void dodajPodpodrucje(Podrucje p){
        podPodrucja.add(p);
    }
    public void dodajUlicu(Ulica u){
        uliceZaPodrucje.add(u);
    }
    public void poveziPodrucja(){
        String poddijelovi[]  = dijelovi.split(",");
        for (String dio : poddijelovi) {
            if(dio.contains("p")){
                //pogledaj pod podrucja
                for(Podrucje p : svaPodrucja){
                    if(p.id.equals(dio)){ 
                        if(p.idNadPodrucja.equals("-1")){
                            this.dodajPodpodrucje(p);
                            p.idNadPodrucja=this.id;
                        }else{
                            Ispis.getInstance().ispisZaDatoteku("Podrucje " + p.naziv + " već pripada nekam!");
                        }
                    }
                }
            }else if(dio.contains("u")){
                //pogledaj pod ulice
                for(Ulica u : Ulica.sveUlice){
                    if(u.id.equals(dio)){ 
                        if(u.idPodrucja.equals("-1")){
                            this.dodajUlicu(u);
                            u.idPodrucja = this.id;
                        }else{
                            Ispis.getInstance().ispisZaDatoteku("Ulica " + u.naziv + " već pripada nekam!");
                        }
                    }
                }
            }else{
                //a valjda bi idjevi trebali imati p i u pa je u suprotnom krivi id
            }   
        }
    }
    public String ispisStrukture(){
        String info = "Podrucje " + naziv + "\n";
        //TODO provjeriti dal ima pod mjesta pa napisati nekaj prikladno
        for(Podrucje p : podPodrucja){
            info += p.naziv + ", ";
        }
        //TODO maknut zadnji zarez recimo
        info += "\n";
        
        for (Ulica u:uliceZaPodrucje){
            info += u.naziv+ ", ";
        }
        //TODO maknut zadnji zarez recimo
        info += "\n\n";
        return info;
    }
    public String getNaziv(){
        return this.naziv;
    }
    /*
    float otpadStaklo;
    float otpadPapir;
    float otpadMetal;
    float otpadBio;
    float otpadMjesano;
    */
    public float getOtpadStaklo(){
        float otpadStaklo = 0;
        for(Podrucje p:podPodrucja){
            otpadStaklo+=p.getOtpadStaklo();
        }
        for(Ulica u: uliceZaPodrucje){
            for(Korisnik k: u.stanovnici){
                    otpadStaklo+=k.getOtpadStaklo();
                }
        }
        return otpadStaklo;
    }
    public float getOtpadPapir(){
        float otpadPapir = 0;
        for(Podrucje p:podPodrucja){
            
            otpadPapir+=p.getOtpadPapir();

        }
        for(Ulica u: uliceZaPodrucje){
            for(Korisnik k: u.stanovnici){
                    otpadPapir+=k.getOtpadPapir();
                }
        }
        return otpadPapir;
    }
    public float getOtpadMetal(){
        float otpadMetal = 0;
        for(Podrucje p:podPodrucja){
            otpadMetal+=p.getOtpadMetal();
        }
        for(Ulica u: uliceZaPodrucje){
            for(Korisnik k: u.stanovnici){
                    otpadMetal+=k.getOtpadMetal();
                }
        }
        return otpadMetal;
    }
    public float getOtpadBio(){
        float otpadBio = 0;
        for(Podrucje p:podPodrucja){
            otpadBio+=p.getOtpadBio();
            
        }
        for(Ulica u: uliceZaPodrucje){
            for(Korisnik k: u.stanovnici){
                    otpadBio+=k.getOtpadBio();
                }
        }
        return otpadBio;
    }
    public float getOtpadMjesano(){
        float otpad = 0;
        for(Podrucje p:podPodrucja){
            otpad+=p.getOtpadMjesano();
            
        }
        for(Ulica u: uliceZaPodrucje){
            for(Korisnik k: u.stanovnici){
                    otpad+=k.getOtpadMjesano();
                }
        }
        return otpad;
    }
    public float getUkupniOtpad(){
        return getOtpadBio()+getOtpadMetal()+getOtpadMjesano()+getOtpadPapir()+getOtpadStaklo();
    }
}
